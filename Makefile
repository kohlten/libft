CFLAGS = -c -g -Wall -Wextra -Wall -Iinclude
CC = gcc
NAME = build/libft.a

CFILES = $(wildcard src/*.c)
OBJ = $(patsubst src/%.c, build/obj/%.o, $(CFILES))

all:	$(NAME)

$(NAME): build $(OBJ)
	@ar rc $(NAME) $(OBJ)
	@ranlib $(NAME)
	@echo "Done"

build:
	-@mkdir build
	-@mkdir build/obj

build/obj/%.o: src/%.c
	@$(CC) $(CFLAGS) -o $@ $<
	@echo "CC $(CFLAGS) -o $@ $<"

clean:
	@rm -rf $(OBJ_FOLDER)

fclean: clean
	@rm -f $(NAME)
	@rm -rf build

re: fclean all

.PHONY: clean fclean re build
