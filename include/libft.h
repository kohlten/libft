/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: astrole <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/19 12:38:14 by astrole           #+#    #+#             */
/*   Updated: 2018/02/19 12:58:37 by astrole          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <string.h>
# include <stdlib.h>
# include <unistd.h>

# define BUFF_SIZE 32
# define LLONG_MAX 9223372036854775807LL
# define LLONG_MIN (-LLONG_MAX - 1LL)
# define ULLONG_MAX 18446744073709551615ULL

typedef struct	s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
	struct s_list	*prev;
}				t_list;

int				get_next_line(const int fd, char **line);

void			*ft_memset		(void *s, int c, size_t n);
void			*ft_bzero		(void *s, size_t n);
void			*ft_memcpy		(void *dst, const void *src, size_t len);
void			*ft_memccpy		(void *dst, const void *src, int c, size_t n);
void			*ft_memmove		(void *dst, const void *src, size_t len);
void			*ft_memchr		(const void *s, int c, size_t n);
char			*ft_strdup		(const char *s1);
char			*ft_strcpy		(char *dst, const char *src);
char			*ft_strncpy		(char *dst, const char *src, size_t len);
char			*ft_strcat		(char *s1, const char *s2);
char			*ft_strncat		(char *s1, const char *s2, size_t nb);
char			*ft_strchr		(const char *s, int c);
char			*ft_strrchr		(const char *s, int c);
char			*ft_strstr		(const char *haystack, const char *needle);
char			*ft_strnstr		(const char *haystack,
	const char *needle, size_t len);
int				ft_strequ		(char const *s1, char const *s2);
int				ft_strnequ		(char const *s1, char const *s2, size_t n);
int				ft_strcmp		(const char *s1, const char *s2);
int				ft_strncmp		(const char *s1, const char *s2, size_t n);
size_t			ft_strlcat		(char *dst, const char *src, size_t siz);
size_t			ft_strlen		(const char *s);
int				ft_memcmp		(const void *s1, const void *s2, size_t n);
int				ft_atoi			(const char *str);
int				ft_isalpha		(int c);
int				ft_isdigit		(int c);
int				ft_isalnum		(int c);
int				ft_isascii		(int c);
int				ft_isprint		(int c);
int				ft_toupper		(int c);
int				ft_tolower		(int c);
void			*ft_memalloc	(size_t size);
void			ft_memdel		(void **ap);
void			ft_strclr		(char *s);
void			ft_strdel		(char **as);
char			*ft_strnew		(size_t size);
void			ft_striter		(char *s, void (*f)(char *));
void			ft_striteri		(char *s, void (*f)(unsigned int, char *));
char			*ft_strmap		(char const *s, char (*f)(char));
char			*ft_strmapi		(char const *s, char (*f)(unsigned int, char));
int				ft_strequ		(char const *s1, char const *s2);
char			*ft_strsub		(char const *s, unsigned int start, size_t len);
char			*ft_strjoin		(char const *s1, char const *s2);
char			*ft_strtrim		(char const *s);
char			**ft_strsplit	(char const *s, char c);
char			*ft_itoa		(long long n);
void			ft_putchar		(char c);
int				ft_putstr		(char const *s);
int				ft_putendl		(char const *s);
int				ft_putnbr		(long n);
int				ft_putunbr		(unsigned long n);
void			ft_putchar_fd	(char c, int fd);
void			ft_putstr_fd	(char const *s, int fd);
void			ft_putendl_fd	(char const *s, int fd);
void			ft_putnbr_fd	(int n, int fd);
int				ft_canfind		(char *haystack, char *needle);
void			*ft_slice		(void *arr, size_t begin, size_t end);
int				ft_isspace		(int c);
char			*ft_strrev		(char *arr, int len);
t_list			*ft_lstnew		(void const *content, size_t content_size);
void			ft_lstdelone	(t_list **alst, void (*del)(void*, size_t));
void			ft_lstdel		(t_list **alst, void (*del)(void *, size_t));
void			ft_lstadd		(t_list **alst, t_list *new);
void			ft_lstiter		(t_list *lst, void (*f)(t_list *elem));
t_list			*ft_lstmap		(t_list *lst, t_list *(*f)(t_list *elem));
void			*ft_realloc		(void *ptr, size_t size, size_t prev_size);
void			ft_putnbrl		(int nbr);
char			*ft_strndup		(const char *s1, size_t num);
char			*ft_strjoinch	(char const *s1, char c);
int				ft_copyuntil	(char **dst, char *src, char c);
int				ft_strisdigit	(char *s);
int				ft_strisnum		(char *str);
double			ft_atof			(char *s);
int				ft_pow			(int x, unsigned int y);
unsigned long   ft_getpointeraddress(const void *p);
unsigned long long ft_reversenumber(unsigned long long num);
int ft_printbase(unsigned long long num, int base, int uppercase, int prefix);
char *ft_itoa_base(long long num, int base, int uppercase, int prefix);
char *ft_utoa_base(unsigned long long num, int base, int uppercase, int prefix);
char *ft_ftoa(long double num);
int	ft_putnstr(char const *s, size_t len);

#endif
