/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: astrole <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/13 11:32:28 by astrole           #+#    #+#             */
/*   Updated: 2018/03/13 11:32:29 by astrole          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	find_len(long n)
{
	int len;

	len = 0;
	while (n > 0)
	{
		n /= 10;
		len++;
	}
	return (len - 1);
}

int			ft_putnbr(long n)
{
	char	digits[23];
	int		i;

	ft_bzero(digits, 23 * sizeof(char));
	if (n < 0 && n != LLONG_MIN)
	{
		n *= -1;
		ft_putchar('-');
	}
	if (n == 0)
		ft_putchar('0');
	else if (n == LLONG_MIN)
		ft_putstr("-9223372036854775808");
	else
	{
		i = find_len(n);
		while (n > 0)
		{
			digits[i--] = (n % 10) + 48;
			n /= 10;
		}
		ft_putstr(digits);
	}
	return (find_len(n));
}

static int	find_ulen(unsigned long n)
{
    int len;

    len = 0;
    while (n > 0)
    {
        n /= 10;
        len++;
    }
    return (len - 1);
}

int			ft_putunbr(unsigned long n)
{
    char	digits[23];
    int		i;

    ft_bzero(digits, 23 * sizeof(char));
    if (n == 0)
        ft_putchar('0');
    else
    {
        i = find_ulen(n);
        while (n > 0)
        {
            digits[i--] = (n % 10) + 48;
            n /= 10;
        }
        ft_putstr(digits);
    }
    return (find_len(n));
}
