//
// Created by Alexander Strole on 2/18/19.
//

#include "libft.h"

static int	find_len(unsigned long long n, int base, int prefix)
{
    int len;

    len = 0;
    while (n > 0)
    {
        n /= base;
        len++;
    }
    if (prefix)
        len += 2;
    return (len);
}

static void get_prefix(char *s, int base, int prefix)
{
    if (prefix) {
        if (base == 2)
            ft_strncpy(s, "0b", 2);
        else if (base == 8)
            ft_strncpy(s, "0", 1);
        else if (base == 16)
            ft_strncpy(s, "0x", 2);
    }
}

char *ft_utoa_base(unsigned long long num, int base, int uppercase, int prefix)
{
    char *output;
    unsigned int len;
    unsigned int i;

    len = find_len(num, base, prefix);
    output = ft_strnew(len);
    if (!output)
        return NULL;
    get_prefix(output, base, prefix);
    i = len - 1;
    while (num > 0)
    {
        if (num % base < 10)
            output[i] = (num % base) + 48;
        else
            if (uppercase)
                output[i] = 65 + ((num % base) - 10);
            else
                output[i] = 97 + ((num % base) - 10);
        num /= base;
        i--;
    }
    output[len] = '\0';
    return output;
}