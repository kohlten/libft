//
// Created by Alexander Strole on 2/11/19.
//

unsigned long long ft_reversenumber(unsigned long long num)
{
    unsigned long long reversed;

    reversed = 0;
    while (num > 0)
    {
        reversed *= 10;
        reversed += num % 10;
        num /= 10;
    }
    return reversed;
}