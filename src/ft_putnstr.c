//
// Created by Alexander Strole on 3/28/19.
//

#include "libft.h"

int	ft_putnstr(char const *s, size_t len)
{
    size_t max_len;

    max_len = ft_strlen(s);
    if (len > max_len)
        len = max_len;
    write(1, s, len);
    return len;
}