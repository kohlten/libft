//
// Created by Alexander Strole on 2/11/19.
//

#include "libft.h"

static int	find_len(unsigned long long n, int base)
{
    int len;

    len = 0;
    while (n > 0)
    {
        n /= base;
        len++;
    }
    return (len);
}

static int print_prefix(int base)
{
    if (base == 2)
    {
        ft_putstr("0b");
        return 2;
    }
    else if (base == 8)
    {
        ft_putstr("0");
        return 1;
    }
    else if (base == 16)
    {
        ft_putstr("0x");
        return 2;
    }
    return 0;
}

int ft_printbase(unsigned long long num, int base, int uppercase, int prefix)
{
    char output[67];
    unsigned int len;
    unsigned int i;

    len = find_len(num, base);
    i = len - 1;
    while (num > 0)
    {
        if (num % base < 10)
            output[i] = (num % base) + 48;
        else
            if (uppercase)
                output[i] = 65 + ((num % base) - 10);
            else
                output[i] = 97 + ((num % base) - 10);
        num /= base;
        i--;
    }
    output[len] = '\0';
    if (prefix)
        len += print_prefix(base);
    ft_putstr(output);
    return len;
}