//
// Created by Alexander Strole on 2/11/19.
//

unsigned long ft_getpointeraddress(const void *p)
{
    return (unsigned long)p;
}