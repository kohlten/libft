//
// Created by Alexander Strole on 2/26/19.
//

#include "libft.h"

int get_len(long double num, unsigned long long *converted)
{
    int len;

    len = 0;
    if (num != 0)
        while (num - (unsigned long long)num < 1)
        {
            if (len == 19)
                break;
            len++;
            num *= 10;
        }
    *converted = (unsigned long long)num;
    return (len);
}

static char *convert_float(long double num)
{
    char *output;
    unsigned long long converted;
    int len;

    len = get_len(num, &converted);
    output = ft_strnew((size_t)len + 1);
    if (!output)
        return (NULL);
    output[0] = '.';
    while (len > 0)
    {
        output[len] = (char)((converted % 10) + 48);
        converted /= 10;
        len--;
    }
    return (output);
}

char *ft_ftoa(long double num)
{
    char *int_part;
    char *float_part;
    char *together;

    int_part = ft_itoa((long long)num);
    if (!int_part)
        return (NULL);
    float_part = convert_float(num - (long double)(long long)num);
    if (!float_part)
        return (NULL);
    together = ft_strjoin(int_part, float_part);
    if (!together)
        return (NULL);
    free(int_part);
    free(float_part);
    return together;
}
