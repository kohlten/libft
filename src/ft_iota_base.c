//
// Created by Alexander Strole on 2/18/19.
//

#include "libft.h"

static int	find_len(long long n, int base, int prefix)
{
    int len;

    len = 0;
    while (n > 0)
    {
        n /= base;
        len++;
    }
    if (prefix)
        len += 2;
    return (len);
}

static void get_prefix(char *s, int base, int prefix)
{
    if (prefix) {
        if (base == 2)
            ft_strncpy(s, "0b", 2);
        else if (base == 8)
            ft_strncpy(s, "0", 1);
        else if (base == 16)
            ft_strncpy(s, "0x", 2);
    }
}

static void convert(char *output, long long num, int len, int base, int uppercase)
{
    int i;

    i = len - 1;
    if (num == 0)
        output[i + 1] = '0';
    else
    {
        while (num > 0)
        {
            if (num % base < 10)
                output[i] = (num % base) + 48;
            else if (uppercase)
                output[i] = 65 + ((num % base) - 10);
            else
                output[i] = 97 + ((num % base) - 10);
            num /= base;
            i--;
        }
    }
}

char *ft_itoa_base(long long num, int base, int uppercase, int prefix)
{
    char *output;
    unsigned int len;
    int neg;

    neg = 1;
    if (num < 0)
    {
        num *= -1;
        neg = -1;
    }
    len = find_len(num, base, prefix);
    output = ft_strnew(len);
    if (!output)
        return NULL;
    get_prefix(output, base, prefix);
    convert(output, num, len, base, uppercase);
    return output;
}